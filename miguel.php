<?php


function regexExtract($text, $regex, $regs, $nthValue)
{
	if (preg_match($regex, $text, $regs)) {
		$result = $regs[$nthValue];
	}
	else {
		$result = "";
	}
	return $result;
}


function printSomething($thing){
	echo '<hr style="border-top: 2px solid green;">';
	if(is_array($thing)){
		var_dump($thing);
	}else{
		echo $thing;
	}
	echo '<hr style="border-top: 2px solid red;">';
}



/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////



//setcookie('ASP_NET_SessionId', '', time() - 3600, '/');

$title = "yesterday";

printSomething('<h2>1: PRIMERA PANTALLA ---- BUSCADOR</h2>');

$url = "http://repertoire.bmi.com/StartPage.aspx";

$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => $url,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_HEADER => 1,
	CURLOPT_HTTPHEADER => array(
		"Content-Length:0",
		),
	));

$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
if ($err) {
	printSomething("cURL Error #:" . $err);
	return;
} else {
	printSomething("No hay error en el paso 1");
	preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $response, $matches);
	$cookies = array();
	foreach($matches[1] as $item) {
		parse_str($item, $cookie);
		$cookies = array_merge($cookies, $cookie);
	}
	printSomething($cookies['ASP_NET_SessionId']);

}




printSomething('<h2>2: SEGUNDA PANTALLA ---- CONDICIONES</h2>');

$url="http://repertoire.bmi.com/Disclaimer.aspx?blnWriter=True&blnPublisher=True&blnArtist=False&blnAltTitles=False";

$regexData = '/__VIEWSTATE\" value=\"(.*)\"/i';
$viewstate = regexExtract($response,$regexData,[],1);
$regexData = '/__VIEWSTATEGENERATOR\" value=\"(.*)\"/i';
$viewstategenerator = regexExtract($response,$regexData,[],1);
$regexData = '/__EVENTVALIDATION\" value=\"(.*)\"/i';
$eventvalidation = regexExtract($response,$regexData,[],1);

printSomething('Datos por response');
printSomething($viewstate);
printSomething($viewstategenerator);
printSomething($eventvalidation);

$data = array(
	'__VIEWSTATE' => $viewstate,
	'__VIEWSTATEGENERATOR' => $viewstategenerator,
	'__EVENTVALIDATION' => $eventvalidation,
	'searchControl$ddlTypeOfSearch' => 'title',
	'searchControl$txtSearchFor' => $title, 
	'searchControl$btnSubmit' => 'Search',
	'searchControl$incWriters' => 'on', 
	'searchControl$incPublishers' => 'on' 
	);


$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => $url,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => $data,
	CURLOPT_HEADER => 1,
	CURLOPT_HTTPHEADER => array(
		"Host: repertoire.bmi.com",
		"Connection: keep-alive",
		"Content-Length: 1",
		"Pragma: no-cache",
		"Cache-Control: no-cache",
		"Origin: http://repertoire.bmi.com",
		"Upgrade-Insecure-Requests: 1",
		"User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36",
		"Content-Type: application/x-www-form-urlencoded",
		"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Referer: http://repertoire.bmi.com/Disclaimer.aspx?blnWriter=True&blnPublisher=True&blnArtist=False&blnAltTitles=False",
		"Accept-Encoding: gzip, deflate",
		"Accept-Language: es-ES,es;q=0.8",
		"Cookie: ASP.NET_SessionId=".$cookies['ASP_NET_SessionId']
		),
	));

$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);

if ($err) {
	printSomething("cURL Error #:" . $err);
	return;
} else {
	printSomething("No hay error");
}



printSomething($response);




printSomething('<h2>3: SEGUNDA PANTALLA ---- ACEPTAR CONDICIONES</h2>');

$url = "http://repertoire.bmi.com/ListView.aspx?torow=25&fromrow=1&page=1";

$data = array(
	'torrow' => 25,
	'fromrow' => 1,
	'page' => 1,
	);

$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => $url,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => $data,
	CURLOPT_HEADER => 1,
	CURLOPT_HTTPHEADER => array(
		"Host: repertoire.bmi.com",
		"Connection: keep-alive",
		"Content-Length: 1",
		"Pragma: no-cache",
		"Cache-Control: no-cache",
		"Origin: http://repertoire.bmi.com",
		"Upgrade-Insecure-Requests: 1",
		"User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36",
		"Content-Type: application/x-www-form-urlencoded",
		"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Referer: http://repertoire.bmi.com/Disclaimer.aspx?blnWriter=True&blnPublisher=True&blnArtist=False&blnAltTitles=False",
		"Accept-Encoding: gzip, deflate",
		"Accept-Language: es-ES,es;q=0.8",
		"Cookie: ASP.NET_SessionId=".$cookies['ASP_NET_SessionId']
		),
	));

$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);

if ($err) {
	printSomething("cURL Error #:" . $err);
	return;
} else {
	printSomething("No hay error");
}














return;


preg_match_all('/id="__VIEWSTATE" value="([^"]+)/mi', $response, $matches);
$viewstate = $matches[1][0];
preg_match_all('/id="__VIEWSTATEGENERATOR" value="([^"]+)/mi', $response, $matches);
$viewstategenerator = $matches[1][0];
preg_match_all('/id="__EVENTVALIDATION" value="([^"]+)/mi', $response, $matches);
$eventvalidation = $matches[1][0];

print_r( "<br>". $cookies['ASP_NET_SessionId'] . '<br>');

$url = "http://repertoire.bmi.com/Disclaimer.aspx?blnWriter=True&blnPublisher=True&blnArtist=False&blnAltTitles=False";
$r = new HttpRequest($url, HttpRequest::METH_POST);
$r->setOptions(['cookies'=>['bmiRepDisclaimerReadNew' => 'T', '__utmt' => '1', 'ASP.NET_SessionId' => $cookies['ASP_NET_SessionId']]]);
$r->addPostFields(['__VIEWSTATE' => $viewstate, '__VIEWSTATEGENERATOR' => $viewstategenerator, '__EVENTVALIDATION' => $eventvalidation, 'btnSubmit' => 'Accept']);

try {
	echo $r->send()->getBody();
} catch (HttpException $ex) {
	echo $ex;
}








return;




foreach ($r->getResponseHeader() as $name => $value) {
	if ($name == "location")
		$redirecturl = $value;
}
echo $redirecturl;

$r1 = new HTTP_Request("http://repertoire.bmi.com".$redirecturl);
$r1->setMethod(HTTP_REQUEST_METHOD_POST);
$r1->addPostData('__VIEWSTATE','/wEPDwUJNzEwNDkzOTExDxYCHhNWYWxpZGF0ZVJlcXVlc3RNb2RlAgEWAgIFD2QWAgIHD2QWCgIBDw8WAh4EVGV4dAUHNzUwLDAwMGRkAgMPDxYCHwEFEW5lYXJseSAxMiBtaWxsaW9uZGQCBQ8PFgIfAQUHNzUwLDAwMGRkAgcPDxYCHwEFEW5lYXJseSAxMiBtaWxsaW9uZGQCCQ8PFgIfAQUTQ29weXJpZ2h0IDE5OTQtMjAxN2RkZNrb7pEYdvbcmjiYrEPWDo+m9VUo7qnwvNrz1L6inaz9');
$r1->addPostData('__VIEWSTATEGENERATOR','758A299B');
$r1->addPostData('__EVENTVALIDATION','/wEdAAP7LEJ3jBG6dcFaLcbHRh1+WV8Y5N3Xps7wejv+rY/VNTzmltaUM7aEAN+g9cP/m12rVaWlUHaoZgTFRrwr532qUGIzhyOBvSRnv7TOqVv/PA==');
//$r1->addCookie("__unam", "73f8473-15bc9f65b44-5db5f335-52");
$r1->addCookie("ASP.NET_SessionId", $cookies['ASP_NET_SessionId']);
$r1->addCookie("bmiRepDisclaimerReadNew", "T");
$r1->addHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
$r1->addHeader("Accept-Encoding","gzip, deflate, sdch");
$r1->addHeader("Accept-Language","en,en-US;q=0.8,es;q=0.6,ja;q=0.4");
$r1->addHeader("Cache-Control","max-age=0");
$r1->addHeader("Connection","keep-alive");
$r1->addHeader("Host","repertoire.bmi.com");
$r1->addHeader("Referer","http://repertoire.bmi.com/Disclaimer.aspx?blnWriter=True&blnPublisher=True&blnArtist=False&blnAltTitles=False");
$r1->addHeader("Upgrade-Insecure-Requests","1");
$r1->addHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36");

//$r1->addCookie("__utmt", "1");
//$r1->addCookie("__utma", "1.1084698184.1493999138.1494260422.1494318370.7");
//$r1->addCookie("__utmb", "1.2.10.1494318370");
//$r1->addCookie("__utmc", "1");
//$r1->addCookie("__utmz", "1.1493999138.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)");
$r1->sendRequest();
$page = $r1->getResponseBody();
print_r($page);




